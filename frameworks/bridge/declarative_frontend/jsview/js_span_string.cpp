#include "frameworks/bridge/declarative_frontend/jsview/js_span_string.h"

#include "frameworks/bridge/common/utils/utils.h"
#include "frameworks/bridge/declarative_frontend/engine/functions/js_function.h"
namespace OHOS::Ace::Framework {

void JSSpanString::JSBind(BindingTarget globalObj)
{
    JSClass<JSSpanString>::Declare("SpanString");
    JSClass<JSSpanString>::Method("setController", &JSTextController::SetController);
    JSClass<JSSpanString>::CustomMethod("getString", &JSTextController::GetString);
    JSClass<JSSpanString>::CustomMethod("getLength", &JSTextController::GetLength);
    JSClass<JSSpanString>::CustomMethod("isEqualToSpanString", &JSTextController::IsEqualToSpanString);
    JSClass<JSSpanString>::CustomMethod("getSubSpanString", &JSTextController::GetSubSpanString);
    JSClass<JSSpanString>::CustomMethod("getSpans", &JSTextController::GetSpans);
    JSClass<JSSpanString>::CustomMethod("getIndex", &JSTextController::GetIndex);
}

void JSSpanString::GetString(const JSCallbackInfo& info)
{
    auto ret = JSRef<JSVal>::Make(JSVal(ToJSValue(spanString_.GetString())));
    info.SetReturnValue(ret);
}

void JSSpanString::GetLength(const JSCallbackInfo& info)
{
    auto ret = JSRef<JSVal>::Make(JSVal(ToJSValue(spanString_.GetLength())));
    info.SetReturnValue(ret);
}

void JSSpanString::IsEqualToSpanString(const JSCallbackInfo& info)
{
    if (info.Length() != 1 || !info[0]->IsObject()) {
        return;
    }
    auto spanString = JSRef<JSObject>::Cast(info[0])->Unwrap<JSSpanString>()->spanString_;
    auto ret = JSRef<JSVal>::Make(JSVal(ToJSValue(spanString_.IsEqualToSpanString(spanString))));
    info.SetReturnValue(ret);
}

void JSSpanString::GetSubSpanString(const JSCallbackInfo& info)
{
    if (info.Length() != 2 || !info[0]->IsNumber() || !info[1]->IsNumber()) {
        return;
    }
    int32_t start = info[0]->ToNumber<int32_t>();
    int32_t length = info[1]->ToNumber<int32_t>();
    auto spanString = spanString_.GetSubSpanString(start, length);

    JSRef<JSObject> obj = JSClass<JSSpanString>::NewInstance();
    auto jsSpanString = Referenced::Claim(obj->Unwrap<JSSpanString>());
    jsSpanString->SetController(spanString);
    info.SetReturnValue(obj);
}

void JSSpanString::GetSpans(const JSCallbackInfo& info)
{
    if (info.Length() != 2 || !info[0]->IsNumber() || !info[1]->IsNumber()) {
        return;
    }
    int32_t start = info[0]->ToNumber<int32_t>();
    int32_t length = info[1]->ToNumber<int32_t>();
    auto spans = spanString_.GetSpans(start, length);
    auto ret = JSRef<JSVal>::Make(JSVal(ToJSValue(spans)));
    info.SetReturnValue(ret);
}

void JSSpanString::GetSpans(const JSCallbackInfo& info)
{
    if (info.Length() != 2 || !info[0]->IsNumber() || !info[1]->IsNumber()) {
        return;
    }
    int32_t start = info[0]->ToNumber<int32_t>();
    int32_t length = info[1]->ToNumber<int32_t>();
    auto spans = spanString_.GetSpans(start, length);
    auto ret = JSRef<JSVal>::Make(JSVal(ToJSValue(spans)));
    info.SetReturnValue(ret);
}

void JSSpanString::GetIndex(const JSCallbackInfo& info)
{
    if (info.Length() != 1 || !info[0]->IsString()) {
        return;
    }
    std::string substring = info[0]->ToString();
    auto index = spanString_.GetIndex(substring);
    auto ret = JSRef<JSVal>::Make(JSVal(ToJSValue(index)));
    info.SetReturnValue(ret);
}

}