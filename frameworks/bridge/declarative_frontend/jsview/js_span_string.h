/*
 * Copyright (c) 2021-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "base/memory/referenced.h"
#include "base/memory/ace_type.h"
#include "bridge/declarative_frontend/engine/bindings_defines.h"
#include "core/components_ng/pattern/text/span/span_string.h"
#include "core/components_ng/pattern/text/span/mutable_span_string.h"

#ifndef FRAMEWORKS_BRIDGE_DECLARATIVE_FRONTEND_JS_VIEW_JS_SPAN_STRING_H
#define FRAMEWORKS_BRIDGE_DECLARATIVE_FRONTEND_JS_VIEW_JS_SPAN_STRING_H

namespace OHOS::Ace::Framework {

class JSSpanString : public virtual AceType {
    DECLARE_ACE_TYPE(JSSpanString, AceType)

public:
    JSSpanString();
    ~JSSpanString() override = default;

    static void JSBind(BindingTarget globalObj);
    void GetString(const JSCallbackInfo& info);
    void GetLength(const JSCallbackInfo& info);
    void IsEqualToSpanString(const JSCallbackInfo& info);
    void GetSubSpanString(const JSCallbackInfo& info);
    void GetSpans(const JSCallbackInfo& info);
    void GetIndex(const JSCallbackInfo& info);
    void SetController(const SpanString& spanString)
    {
        spanString_ = spanString;
    }

private:
    ACE_DISALLOW_COPY_AND_MOVE(JSSpanString);
    SpanString spanString_; // SpanString有场景需要返回到前端
};

class JSMutableSpanString final : JSSpanString {
    DECLARE_ACE_TYPE(JSMutableSpanString, JSSpanString)

public:
    JSMutableSpanString() = default;
    ~JSMutableSpanString() override = default;

    static void JSBind(BindingTarget globalObj);

private:
    ACE_DISALLOW_COPY_AND_MOVE(JSMutableSpanString);
    WeakPtr<MutableSpanString> mutableSpanString_;
};

} // namespace OHOS::Ace::Framework
#endif // FRAMEWORKS_BRIDGE_DECLARATIVE_FRONTEND_JS_VIEW_JS_SPAN_STRING_H
