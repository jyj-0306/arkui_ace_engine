/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class FrameNode {
  private renderNode_: RenderNode;
  private baseNode_ : BaseNode;
  protected nodePtr_ : number | null;
  constructor(uiContext: UIContext, type: string) {
    this.renderNode_ = new RenderNode('FrameNode');
    if (type === 'BuilderNode') {
      return;
    }
    this.baseNode_ = new BaseNode(uiContext);
    this.nodePtr_ =  this.baseNode_.createRenderNode(this);
    this.renderNode_.setNodePtr(this.nodePtr_);
    this.renderNode_.setBaseNode(this.baseNode_);
  }
  getRenderNode(): RenderNode | null {
    if (
      this.renderNode_ !== undefined &&
      this.renderNode_ !== null &&
      this.renderNode_.getNodePtr() !== null
    ) {
      return this.renderNode_;
    }
    return null;
  }
  setNodePtr(nodePtr: number | null) {
    this.nodePtr_ = nodePtr;
    this.renderNode_.setNodePtr(nodePtr);
  }
  setBaseNode(baseNode: BaseNode | null) {
    this.baseNode_ = baseNode;
    this.renderNode_.setBaseNode(baseNode);
  }
  getNodePtr(): number | null {
    return this.nodePtr_;
  }
  dispose() {
    this.baseNode_.dispose()
  }
}
