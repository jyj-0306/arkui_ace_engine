/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "core/components_ng/pattern/text/span/mutable_span_string.h"

namespace OHOS::Ace {
void MutableSpanString::SortSpans(std::list<RefPtr<SpanBase>>& spans)
{
    spans.sort([](const RefPtr<SpanBase>& a, const RefPtr<SpanBase>& b){
        return a->GetStartIndex() < b->GetStartIndex();
    });
}

// precondition: a->GetStartIndex() <= b->GetStartIndex()
bool MutableSpanString::CanMerge(const RefPtr<SpanBase>& a, const RefPtr<SpanBase>& b)
{
    return a->GetEndIndex() >= b->GetStartIndex() && a->IsAttributesEqual(b);
}

void MutableSpanString::MergeIntervals(std::list<RefPtr<SpanBase>>& spans)
{
    auto it = spans.begin();
    while (it != spans.end()) {
        auto current = it++;
        if (it != spans.end() && CanMerge(*current, *it) ) {
            (*current)->UpdateStartIndex(std::min((*current)->GetStartIndex(), (*it)->GetStartIndex()));
            (*current)->UpdateEndIndex(std::max((*current)->GetEndIndex(), (*it)->GetEndIndex()));
            spans.erase(it++);
            if (it == spans.end()) {
                break;
            }
            it = current;
        }
    }
}

void MutableSpanString::AddSpan(const RefPtr<SpanBase>& span)
{
    if (!span) {
        return;
    }
    
    if (!CheckRange(span->GetStartIndex(), span->GetLength())) {
        return;
    }
    auto spans = spansMap_[span->GetSpanType()];

    auto start = span->GetStartIndex();
    auto end = span->GetEndIndex();
    std::list<RefPtr<SpanBase>> newSpans;
    for (auto it = spans.begin(); it != spans.end();) {
        auto interval = (*it)->GetIntersectionInterval({start, end});
        
        if (!interval) {
            ++it;
            continue;
        }
        auto oldStart = (*it)->GetStartIndex();
        auto oldEnd = (*it)->GetEndIndex();
        if (interval->first == oldStart && interval->second == oldEnd) {
            it = spans.erase(it);
            continue;
        }

        if (oldStart < interval->first && interval->second < oldEnd) {
            newSpans.emplace_back((*it)->GetSubSpan(oldStart, interval->first));
            newSpans.emplace_back((*it)->GetSubSpan(interval->second, oldEnd));
            it = spans.erase(it);
        }

        if (oldEnd > interval->second) {
            (*it)->UpdateStartIndex(interval->second);
            ++it;
            continue;
        }

        if (interval->first > oldStart) {
            (*it)->UpdateEndIndex(interval->first);
            ++it;
            continue;
        }
    }
    spans.emplace_back(newSpans);
    spans.emplace_back(span);
    SortSpans(spans);
    MergeIntervals(spans);
    spansMap_[span->GetSpanType()] = spans;
}

void MutableSpanString::RemoveSpan(int32_t start, int32_t length, SpanType span)
{
    
}

void MutableSpanString::ReplaceString(int32_t start, int32_t length, const std::string& other)
{
    int end = start + length;
    int offset = other.length() - length;
    // 1. change text_ to text_[:start] + other + text_[end:]
    auto text_ = GetString();
    SetString(text_.substr(0, start) + other + text_.substr(end));
    end += offset;
    // 2. change span item
    for (auto iter = spansMap_.begin(); iter != spansMap_.end(); ++iter) {
        auto spans = spansMap_[iter->first];
        for (auto it = spans.begin(); it != spans.end();) {
            if ((*it)->GetStartIndex() > start) {
                (*it)->UpdateStartIndex((*it)->GetStartIndex() + offset);
            }
            if ((*it)->GetEndIndex() > start) {
                (*it)->UpdateEndIndex((*it)->GetEndIndex() + offset);
            }
            auto interval = (*it)->GetIntersectionInterval({start, end});
            if (!interval) {
                ++it;
                continue;
            }
            auto spanStart = (*it)->GetStartIndex();
            auto spanEnd = (*it)->GetEndIndex();
            if (interval->first == spanStart && interval->second == spanEnd) {
                it = spans.erase(it);
                continue;
            }
            if (interval->first >= spanStart) {
                (*it)->UpdateEndIndex(std::max(interval->second, spanEnd));
            } else { // interval->first < spanStart
                (*it)->UpdateStartIndex(interval->second);
            }
            ++it;
        }
    }
    KeepSpansOrder();
}

void MutableSpanString::InsertString(int32_t start, const std::string& other)
{
    ReplaceString(start, 0, other);
}

void MutableSpanString::RemoveString(int32_t start, int32_t length)
{
    ReplaceString(start, length, "");
}

void MutableSpanString::ClearAllSpans()
{
    for (auto it = spansMap_.begin(); it != spansMap_.end(); ++it) {
        auto spans = spansMap_[it->first];
        spans.clear();
    }
}

void MutableSpanString::KeepSpansOrder()
{
    for (auto it = spansMap_.begin(); it != spansMap_.end(); ++it) {
        auto spans = spansMap_[it->first];
        SortSpans(spans);
        MergeIntervals(spans);
    }
}

void MutableSpanString::ReplaceSpanString(int32_t start, int32_t length, const SpanString& spanString)
{
    ReplaceString(start, length, spanString.GetString());
    auto spanStringSpansMap = spanString.GetSpansMap();
    for (auto it = spanStringSpansMap.begin(); it != spanStringSpansMap.end(); ++it) {
        auto spans = spansMap_[it->first];
        auto spanStringSpans = spanStringSpansMap[it->first];
        for (auto it2 = spanStringSpans.begin(); it2 != spanStringSpans.end(); ++it2) {
            spans.emplace_back((*it2)->GetSubSpan((*it2)->GetStartIndex() + start, (*it2)->GetEndIndex() - (*it2)->GetStartIndex()));
        }
    }
    KeepSpansOrder();
}

void MutableSpanString::InsertSpanString(int32_t start, const SpanString& spanString)
{
    ReplaceSpanString(start, 0, spanString);
}

void MutableSpanString::AppendSpanString(const SpanString& spanString)
{
    SetString(GetString() + spanString.GetString());
    int offset = GetLength();
    auto spanStringSpansMap = spanString.GetSpansMap();
    for (auto it = spanStringSpansMap.begin(); it != spanStringSpansMap.end(); ++it) {
        auto spans = spansMap_[it->first];
        auto spanStringSpans = spanStringSpansMap[it->first];
        for (auto it2 = spanStringSpans.begin(); it2 != spanStringSpans.end(); ++it2) {
            spans.emplace_back((*it2)->GetSubSpan((*it2)->GetStartIndex() + offset, (*it2)->GetEndIndex() - (*it2)->GetStartIndex()));
        }
    }
    KeepSpansOrder();
}
}