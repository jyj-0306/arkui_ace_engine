/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FOUNDATION_ACE_FRAMEWORKS_CORE_COMPONENTS_NG_PATTERNS_TEXT_SPAN_SPAN_STRING_H
#define FOUNDATION_ACE_FRAMEWORKS_CORE_COMPONENTS_NG_PATTERNS_TEXT_SPAN_SPAN_STRING_H

#include "base/geometry/dimension.h"
#include "base/memory/ace_type.h"
#include <list>
#include <map>
#include <string>
#include <vector>

#include "base/memory/referenced.h"
#include "core/components_ng/pattern/text/span_node.h"
#include "core/components_ng/pattern/text/span/span_objects.h"

namespace OHOS::Ace {

class ACE_EXPORT SpanString : public AceType {
    DECLARE_ACE_TYPE(SpanString, AceType);

public:
    SpanString(const SpanString& other);
    explicit SpanString(std::string text);
    void BindAttachment(const AttachmentImage& attachmentImage);
    std::string GetString() const;
    int32_t GetLength() const;

    void SetString(std::string text) {
        text_ = text;
    }
    std::map<SpanType, std::list<RefPtr<SpanBase>>>& GetSpansMap() const;
    bool IsEqualToSpanString(const SpanString& other) const;
    SpanString GetSubSpanString(int32_t start, int32_t length) const;
    std::vector<RefPtr<SpanBase>> GetSpans(int32_t start, int32_t length) const;
    int32_t GetIndex(const std::string& subString) const;
    bool operator==(const SpanString& other) const;
    SpanString& operator=(const SpanString& other);

protected:
    bool CheckRange(int32_t start, int32_t length) const;

    std::string text_;
    // std::list<RefPtr<SpanBase>> spans_;
    std::map<SpanType, std::list<RefPtr<SpanBase>>> spansMap_;
};
} // namespace OHOS::Ace

#endif // FOUNDATION_ACE_FRAMEWORKS_CORE_COMPONENTS_NG_PATTERNS_TEXT_SPAN_SPAN_STRING_H
