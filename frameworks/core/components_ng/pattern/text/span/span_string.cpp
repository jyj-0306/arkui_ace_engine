/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "core/components_ng/pattern/text/span/span_string.h"

namespace OHOS::Ace {
bool SpanString::CheckRange(int32_t start, int32_t length) const
{
    if (length <= 0) {
        return false;
    }

    auto len = GetLength();
    auto end = start + length;

    if (start > len || end > len) {
        return false;
    }

    if (start < 0) {
        return false;
    }

    return true;
}

SpanString::SpanString(const SpanString& other)
{
    *this = other;
}

SpanString::SpanString(std::string text) : text_(text) {}

void SpanString::BindAttachment(const AttachmentImage& attachmentImage) {}

std::string SpanString::GetString() const
{
    return text_;
}

int32_t SpanString::GetLength() const
{
    return text_.length();
}

bool SpanString::IsEqualToSpanString(const SpanString& other) const
{
    return *this == other;
}

SpanString SpanString::GetSubSpanString(int32_t start, int32_t length) const
{
    if (!CheckRange(start, length)) {
        SpanString span("");
        return span;
    }
    int32_t end = start + length;
    SpanString span(text_.substr(start, end));
    std::map<SpanType, std::list<RefPtr<SpanBase>>> subMap;
    for (auto map = spansMap_.begin(); map != spansMap_.end(); ++map) {
        auto spans = map->second;
        std::list<RefPtr<SpanBase>> subList;
        for (auto itr = spans.begin(); itr != spans.end(); ++itr) {
            int32_t spanStart = (*itr)->GetStartIndex();
            int32_t spanEnd = (*itr)->GetEndIndex();
            if (!(start <= spanStart && spanStart < end) || !(start <= spanEnd && spanEnd < end)) {
                continue;
            }
            spanStart = spanStart <= start ? 0 : spanStart - start;
            spanEnd = spanEnd < end ? spanStart - start : end - start;
            subList.push_back((*itr)->GetSubSpan(spanStart, spanEnd)); // 调用还未实现的subspanbase
        }
        if (!subList.empty()) {
            subMap.emplace(map->first, subList);
        }
    }
    span.spansMap_ = subMap;
    return span;
}

std::vector<RefPtr<SpanBase>> SpanString::GetSpans(int32_t start, int32_t length) const
{
    std::vector<RefPtr<SpanBase>> res;
    if (!CheckRange(start, length)) {
        return res;
    }
    int32_t end = start + length;
    for (auto map = spansMap_.begin(); map != spansMap_.end(); ++map) {
        for (auto itr = map->second.begin(); itr != map->second.end(); ++itr) {
            auto spanBase = *itr;
            if ((start <= spanBase->GetStartIndex() && spanBase->GetStartIndex() < end) ||
                (start <= spanBase->GetEndIndex() && spanBase->GetEndIndex() < end)) {
                res.push_back(spanBase);
            }
        }
    }
    return res;
}

int32_t SpanString::GetIndex(const std::string& subString) const
{
    return text_.find(subString);
}

bool SpanString::operator==(const SpanString& other) const
{
    if (text_ != other.text_ || spansMap_.size() != other.spansMap_.size()) {
        return false;
    }
    for (auto itr = spansMap_.begin(); itr != spansMap_.end(); ++itr) {
        SpanType type = itr->first;
        if (other.spansMap_.find(type) == other.spansMap_.end()) {
            return false;
        }
        auto spans = itr->second;
        auto spansOther = other.spansMap_.find(type)->second;
        if (spans.size() != spansOther.size()) {
            return false;
        }
        for (auto span = spans.begin(), spanOther = spansOther.begin();
             span != spans.end() && spanOther != spansOther.end(); ++span, ++spanOther) {
            if (!(*span)->IsAttributesEqual(*spanOther) || (*span)->GetEndIndex() != (*spanOther)->GetEndIndex() ||
                (*span)->GetStartIndex() != (*span)->GetStartIndex()) {
                return false;
            }
        }
    }
    return true;
}

SpanString& SpanString::operator=(const SpanString& other)
{
    text_ = other.text_;
    spansMap_.clear();
    for (auto itr = other.spansMap_.begin(); itr != other.spansMap_.end(); ++itr) {
        spansMap_.emplace(itr->first, itr->second);
    }
    return *this;
}

} // namespace OHOS::Ace